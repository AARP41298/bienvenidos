<?php

namespace App\Observers;

use App\Models\Property;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


//este observador se debe declarar en app/providers/eventserviceprovider
class PropertyObserver {
    /**
     * Handle the Property "created" event.
     *
     * @param \App\Models\Property $property
     * @return void
     */
    public function creating(Property $property) {
        //no usa el observer si se agregan datos desde la consola (sobre todo para los seeders)
        if (!\App::runningInConsole()) {
            $property->user_id = auth()->user()->id;
        }
    }

    /**
     * Handle the Property "updated" event.
     *
     * @param \App\Models\Property $property
     * @return void
     */
    public function updated(Property $property) {
        //
    }

    /**
     * Handle the Property "deleted" event.
     *
     * @param \App\Models\Property $property
     * @return void
     */
    //se activa despues de eliminar
    public function deleted(Property $property) {

    }

    //se activa antes de eliminar
    public function deleting(Property $property) {
        //Solamente las relaciones, faltan los archivos
        //Ahora con "observer" en imagenes
        //no funciona
        //$property->images()->delete();

//        $images = $property->images;
//        foreach ($images as $image) {
//            Storage::disk('public')->delete($image->url);
//            $image->delete();
//        }

        $property->images->each->delete();
    }

    /**
     * Handle the Property "restored" event.
     *
     * @param \App\Models\Property $property
     * @return void
     */
    public function restored(Property $property) {
        //
    }

    /**
     * Handle the Property "force deleted" event.
     *
     * @param \App\Models\Property $property
     * @return void
     */
    public function forceDeleted(Property $property) {
        //
    }
}
