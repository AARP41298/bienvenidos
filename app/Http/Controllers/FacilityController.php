<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Facility;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FacilityController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $facilities = Facility::all();
/*
        $municipios = Estado::find(5)->municipios->all();
        $res = new Collection([]);
        $resArr = [];
        foreach ($municipios as $municipio) {
            $res = $res->merge($municipio->localidades);
            $resArr = array_merge($resArr,$municipio->localidades->pluck('name')->toArray());
        }
        return $resArr;
        return $res->count();
        return $municipios;

        return Estado::find(1)->municipios->find(5)->localidades->pluck('name');
        return Estado::all();
        */

        return Inertia::render('App/Facility', [
            'facilitiesProp' => $facilities,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Facility $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility) {
        //
    }
}
