<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Localidad;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EstadoController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $estados = Estado::all();
        return Inertia::render('App/Pais', [
            'estados' => $estados
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Estado $estado) {
        $municipios = Municipio::where('estado_id', $estado->id)->orderBy('name')->get();
        return Inertia::render('App/Estado', [
            'estado' => $estado,
            'municipios' => $municipios
        ]);
    }

    public function municipio(Estado $estado, Municipio $municipio) {
//        $localidades = Localidad::where('municipio_id', $municipio->id)->orderBy('name')->get();
//        return Inertia::render('App/Municipio', [
//            'localidades' => $localidades
//        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
