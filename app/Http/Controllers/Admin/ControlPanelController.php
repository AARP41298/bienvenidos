<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Property;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ControlPanelController extends Controller {
    public function __construct() {
        //laravel permission
        $this->middleware('can:controlpanel.index')->only('index');
        $this->middleware('can:controlpanel.contact')->only('contact');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $properties = Property::where('user_id', auth()->user()->id)->get()->sortBy('name')->values();
        foreach ($properties as $property) {
            $property->cover;
            $property->estado;
            $property->municipio;
            $property->status;
        }

        return Inertia::render('App/Admin/Property/Index', [
            'properties' => $properties,
        ]);
    }

    public function home() {
        return Inertia::render('App/Admin/Property/Home',
            []);
    }

    public function adminIndex() {
        $properties = Property::all()->sortBy('name')->values();
        foreach ($properties as $property) {
            $property->cover;
            $property->estado;
            $property->municipio;
            $property->status;
        }
        return Inertia::render('App/Admin/Property/AdminIndex', [
            'properties' => $properties->paginate(10),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    //cambiar a PropertyController?
    public function edit(Property $property) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function contact() {
        return Inertia::render('App/Admin/Contact', [
            'contact' => Contact::find(auth()->user()->id)
        ]);
    }
}
