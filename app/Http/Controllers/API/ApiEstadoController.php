<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\Models\Localidad;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiEstadoController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function all() {
        return Estado::all();
    }

    public function municipios(Estado $estado) {
        return Municipio::where('estado_id', $estado->id)->orderBy('name')->get();
    }

    public function localidades(Estado $estado, Municipio $municipio) {
        //debido a que los municipios no tienen coordenadas, se obtiene el mayor y menor valor de longitud
        //y latitud, para asi poder obtener un punto medio entre todos los puntos de las localidades
//        return DB::table('localidades')
//            ->select(DB::raw('((min(lat) + max(lat))/2) as lat,((min(lng) + max(lng))/2) as lng'))
//            ->where('municipio_id', $municipio->id)->get();

        return Localidad::where('municipio_id', $municipio->id)->orderBy('name')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
