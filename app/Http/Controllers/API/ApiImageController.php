<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Property;
use Illuminate\Http\Request;

use DateTime;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as IMS;

class ApiImageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $property = Property::where('slug', $request->propertySlug)->first();
        if ($property->user_id != auth()->user()->id) {
            abort(404);
        }
        $images = $property->images;
        $order = $images->max('order') + 1;
        $length = $images->count();
        //limitar numero de imagenes por post
        if ($length > 20) {
            abort(404);
        }

        $image = $request->file('image');

        $path = public_path('storage/properties/');
        $datetime = new DateTime();
        $filename = uniqid() . $datetime->format('dmYHisu') . '.' . 'jpg';
        IMS::make($image)
            ->orientate()
            ->resize(1920, 1920, function ($constraint) {
                $constraint->aspectRatio();
            })
            //->encode('jpg', 75)
            ->save($path . $filename, 75);
        $url = 'properties/' . $filename;

        return Image::create([
            'property_id' => $property->id,
            'url' => $url,
            'order' => $order,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function property(Property $property) {
        return $property->images;
    }

    public function show(Image $image) {
        return $image;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $requests, Property $property) {
        if ($property->user_id != auth()->user()->id) {
            abort(404);
        }

        //transforma a array
        $requests = $requests->all();

        foreach ($requests as $request) {
            //se podria transformar de array a objeto para leer $request->id
            $image = Image::find($request['id']);
            $image->order = $request['order'];
            $image->save();
        }
        return $property->images;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image) {
        $property = Property::where('id', $image->property_id)->first();
        if ($property->user_id != auth()->user()->id) {
            abort(404);
        }
        //se elimina el archivo en el modelo
        $image->delete();
        return response('Deleted', 200);
    }
}
