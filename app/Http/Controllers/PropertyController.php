<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyRequest;
use App\Models\Contact;
use App\Models\Estado;
use App\Models\Image;
use App\Models\Municipio;
use App\Models\Property;
use App\Models\Uso;
use App\Models\Facility;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;

use DateTime;
use Intervention\Image\ImageManagerStatic as IMS;
use Symfony\Component\HttpKernel\Exception\HttpException;

//activar gd, mbstring y exif

class PropertyController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //$properties = Property::all();
        //$properties = Property::paginate(10);

        //$properties = Property::orderBy('id', 'DESC')->paginate(15);

        //return $properties;
        //Uso::find(2)->categories()->get('uso_id')
        /*
        return Inertia::render('App/Property/Index', [
            //'properties' => $properties->data,
            'properties' => $properties,

            //'paginate' => $properties
        ]);*/
        $count = Property::whereIn('status_id', [1, 2])->count();
        return Inertia::render('App/Property/Index', [
            'count' => $count
        ]);
    }

    public function create() {

        return Inertia::render('App/Admin/Property/Create', [
            'editing' => false,
        ]);
    }


    public function paginate(int $size) {
        //$properties = Property::paginate($size, ['name', 'id', 'slug']);
        //$properties = Property::paginate(10, ['name', 'id']);
        return Property::paginate($size, ['name', 'id', 'slug']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
//    //forma 1
//    public function store(Request $request) {
//        $request->merge(['slug' => Str::slug($request->name)]);
//        app(PropertyRequest::class);
//    }


    //forma 2 en el objeto
    public function store(PropertyRequest $request) {
        //en vue se mandan como objecto
        //pero inertia los recibe como array assosiativo


        //solo se pueden crear publicaciones con estos status
        if (in_array($request->status['slug'], ['en-renta', 'en-venta'])) {
            $status = Status::where('slug', $request->status['slug'])->first()->id;
        } else abort(404);

        //para evitar incoherencias, se verifica que el municipio sea parte del estado
        $municipio = Municipio::
        where('estado_id', Estado::where('id', $request->estado['id'])->first()->id)
            ->where('id', $request->municipio['id'])
            ->first();

        $property = Property::create([
            'user_id' => auth()->user()->id,
            'status_id' => $status,
            'category_id' => $request->category['id'],
            'name' => $request->name,
            'slug' => $request->slug,
            'value' => $request->value,
            'metros2' => $request->metros2,
            'lat' => $request->location['lat'],
            'lng' => $request->location['lng'],

            'pub_lat' => $request->publicLocation['latitude'],
            'pub_lng' => $request->publicLocation['longitude'],
            'public_gps' => $request->public_gps,

            'estado_id' => $municipio->estado_id,
            'municipio_id' => $municipio->id,
            'CP' => $request->CP,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'num_ext' => $request->num_ext,
            'num_int' => $request->num_int,
            'description' => $request->description,

            //visits default 0 database
        ]);

        //        Property::find(1)->facilities()->sync([
        //            1 => ['count' => 9],
        //            2 => ['count' => 8],
        //            3 => ['count' => 7]
        //        ]);


        $property->facilities()->sync($this->facilities($request->facilities));

        $path = public_path('storage/properties/');
        $order = 0;

        foreach ($request->images as $image) {
            $datetime = new DateTime();
            $filename = uniqid() . $datetime->format('dmYHisu') . '.' . 'jpg';

            //comprimir imagen y guardar archivo
            IMS::make($image)
                ->orientate()
                ->resize(1920, 1920, function ($constraint) {
                    $constraint->aspectRatio();
                })
                //->encode('jpg', 75)
                ->save($path . $filename, 75);
            $url = 'properties/' . $filename;
            //guardar imagen en la DB
            Image::create([
                'property_id' => $property->id,
                'url' => $url,
                'order' => $order,
            ]);
            $order++;
        }
        return Redirect::route('property.show', $property);
    }


    public function search(Request $request) {
        //obtengo TODAS las propiedades del uso requerido por slug
        if ($request->uso != null) {
            $properties = Uso::where('slug', $request->uso)->get()->pluck('categories')->flatten()->pluck('properties')->flatten();
        } else {
            $properties = Uso::all()->pluck('categories')->flatten()->pluck('properties')->flatten();
        }


        if ($request->uso = 'domestico') {

            if ($request->selHa != null) {
                $habReq = $request->selHa;
                $properties = $properties->filter(function ($property) use ($habReq) {
                    //obtengo cuantas habitaciones hay en la propiedad
                    //$habProp = $property->facilities->where('slug', 'habitaciones')->all()[2]->pivot->count;
//Property::find(4)->facilities->values()->flatten()->where('slug','habitaciones')->toArray()[2]['pivot']['count']


                    $habProp = $property->facilities->where('slug', 'habitaciones')->toArray();
                    if (array_key_exists(2, $habProp)) {
                        $habProp = intval($habProp[2]['pivot']['count']);
                        //la propiedad debe de tener más habitaciones que las requeridas
                        if ($habProp >= $habReq) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        //si no hay habitacion no se queda
                        return false;
                    }

                });
            }
            /*
                        $haystack = $request->selCa;
                        $properties = $properties->filter(function ($property) use ($haystack) {
                            $needle = $property->category->slug;
                            $keep = false;
                            if (in_array($needle, $haystack)) {
                                $keep = true;
                            }
                            return $keep;
                        });*/
        }

        //filtro conforme lo que me van pasando

        if ($request->estado != null) {
            if ($request->municipio != null) {
                $properties = $properties->where('municipio_id', '=', $request->municipio);
            } else {
                $properties = $properties->where('estado_id', '=', $request->estado);
            }
        }

        //se escogio 1 o * status
        if ($request->selSta != null) {
            //quiero agujas
            $needles = array_filter($request->selSta, function ($needle) {
                //solo las agujas que esten en el pajar se van a quedar
                $haystack = ['en-venta', 'en-renta', 'vendido', 'rentando'];
                if (in_array($needle, $haystack)) {
                    return true;
                } else {
                    return false;
                }
            });
            //Category::whereIn('slug',['terreno','oficina'])->get()->modelKeys();
            //Property::whereIn('category_id',Category::whereIn('slug',['terreno','oficina'])->get()->modelKeys())->get()
            $statuses_id = Status::whereIn('slug', $needles)->get()->modelKeys();
            $properties = $properties->whereIn('status_id', $statuses_id);
        } else {
            //en venta y en renta
            $properties = $properties->whereIn('status_id', [1, 2]);
        }

        if ($request->precio != null) {
            $properties = $properties->where('value', '<=', $request->precio);
        }
        if ($request->preciomin != null) {
            $properties = $properties->where('value', '>=', $request->preciomin);
        }
        $maxvalprop = $properties->max('value');

        switch ($request->ordenar) {
            case 'menorprecio':
                $properties = $properties->sortBy('value')->values();
                break;
            case 'mayorprecio':
                $properties = $properties->sortByDesc('value')->values();
                break;
        }


        //instalaciones de las propiedades resultantes
        $facilities = $properties->pluck('facilities')->flatten()
            ->unique('id')->sortBy('name')->values();
        //categorias de las propiedades resitlantes
        $categories = $properties->pluck('category')->flatten()
            ->unique('id')->sortBy('name')->values();


        if ($request->selFa != null) {
            $needles = $request->selFa;
            $properties = $properties->filter(function ($property) use ($needles) {
                //obtego todos los slug de la propiedad en array
                $pajar = $property->facilities->map->only('slug')->flatten()->toArray();

                //busco 1 a 1 el slug del request en los slugs de la propiedad
                //se debe cumplir con TODAS las instalaciones
                $keep = 0;
                foreach ($needles as $needle) {
                    if (in_array($needle, $pajar)) {
                        $keep++;
                    }
                }
                if ($keep == count($needles)) {
                    return true;
                } else {
                    false;
                }

                /*
                                //con 1 que se cumpla
                                $keep = false;
                                foreach ($needles as $needle) {
                                    if (in_array($needle, $pajar)) {
                                        $keep = true;
                                    }
                                }
                                return $keep;*/
            });
        }

        if ($request->selCa != null) {
            $haystack = $request->selCa;
            $properties = $properties->filter(function ($property) use ($haystack) {
                $needle = $property->category->slug;
                $keep = false;
                if (in_array($needle, $haystack)) {
                    $keep = true;
                }
                return $keep;
            });
        }


        // Property::find(2)->facilities->map->only('slug')->flatten()->toArray() //['agua','luz','espacio']


        //$properties = $properties->sortBy('radius')->values();

        foreach ($properties as $property) {
            if ($property->public_gps == 0) {
                $property->lat = $property->pub_lat;
                $property->lng = $property->pub_lng;

                $property->calle = null;
                $property->num_int = null;
                $property->num_ext = null;
                $property->colonia = null;
                $property->CP = null;
                //$property->localidad = null;
            }
            //agregar la relacion 1 por 1
            $property->images;
            $property->cover;
            $property->estado;
            $property->municipio;
            $property->facilities;

        }

        /*
                //a cada propiedad se llama property
                $properties = $properties->filter(function ($property, $key) {
                    //obtengo el array de los ids de las instalaciones de la propiedad
                    $ids = $property->facilities->modelKeys();
                    //ejemplo 12, debo poner el array del request en formato [1,2,3]
                    if (in_array(12, $ids)) {
                        return true;
                    } else {
                        return false;
                    }

                    foreach ($property->facilities as $facility) {
                        // if ($facility->id == 12) {
                        if (in_array(12, $facility->id)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });*/


        return Inertia::render('App/Property/Listing', [
            'request' => $request,
            //aqui mando mis valores, estan paginados, por eso es que debo accesar a ellos con
            //data
            'properties' => $properties->paginate(10),
            'maxvalprop' => floatval($maxvalprop),
            'facilities' => $facilities,
            'categories' => $categories,
        ]);
    }
    /*
        public function searchFeo(Request $request) {
            $property->radius = acos(cos($centerLat * (PI() / 180)) *
                    cos($centerLng * (PI() / 180)) *
                    cos($lat * (PI() / 180)) *
                    cos($lng * (PI() / 180))
                    +
                    cos($centerLat * (PI() / 180)) *
                    sin($centerLng * (PI() / 180)) *
                    cos($lat * (PI() / 180)) *
                    sin($lng * (PI() / 180))
                    +
                    sin($centerLat * (PI() / 180)) *
                    sin($lat * (PI() / 180))
                ) * 6371;
            //6371 - radio de la tierra en km
            //3959 - en millas

            //FUNCIONA PERFECTO //all opcional(?)
            //$properties = $properties->sortByDesc('user_id')->values()->all();
            //$properties = $properties->sortBy('user_id')->values()->all();
            $properties = $properties->sortBy('radius')->values();
        }
    */

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Property $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property) {

        if ($property->status->slug == 'baneado' ||
            $property->status->slug == 'oculto') {
            return abort(404);
        }

        //$localidad = null;


        //0 apagar ubicacion exacta, usar ubicacion publica
        if ($property->public_gps === '0') {
            $property->lat = $property->pub_lat;
            $property->lng = $property->pub_lng;

            $property->calle = null;
            $property->num_int = null;
            $property->num_ext = null;
            $property->colonia = null;
            $property->CP = null;

            //$property->localidad = null;
        } else {
            //$localidad = $property->localidad->name;
        }
        $estado = $property->estado;
        $municipio = $property->municipio;

        $facilities = $property->facilities;
        $images = $property->images;


        if (count($images) == 0) {
            $images = [
                (object)['url' => '404image.jpg']
            ];
        }

        return Inertia::render('App/Property/Show2', [
            'property' => $property,
            'contact' => Contact::where('user_id', $property->user_id)->get()[0],
            'estado' => $estado,
            'municipio' => $municipio,
            //'localidad' => $localidad,
            'facilities' => $facilities,
            'images' => $images,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Property $property
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, Property $property) {
    public function visits(int $property) {
        $property = Property::findorfail($property);
        $property->visits = $property->visits + 1;
        $property->save();
        return response('gracias por su visita', 200);
    }

    public function edit(Property $property) {
        if ($property->user_id != auth()->user()->id) {
            abort(404);
        }
        $property->status;
        $property->facilities;
        $property->status;
        $property->category->uso;
        $property->estado;
        $property->municipio;
        $property->images;

        return Inertia::render('App/Admin/Property/Create', [
            'editing' => true,
            'property' => $property,
        ]);
    }

    public function update(PropertyRequest $request, Property $property) {
        //solo se pueden editar publicaciones con estos status
        if (in_array($request->status['slug'], ['en-renta', 'en-venta', 'vendido', 'rentando', 'oculto'])) {
            $status = Status::where('slug', $request->status['slug'])->first()->id;
        } else abort(404);

        //para evitar incoherencias, se verifica que el municipio sea parte del estado
        $municipio = Municipio::
        where('estado_id', Estado::where('id', $request->estado['id'])->first()->id)
            ->where('id', $request->municipio['id'])
            ->first();


        $property->update([
//            'user_id' => auth()->user()->id,
            'status_id' => $status,
            'category_id' => $request->category['id'],
            'name' => $request->name,
            'slug' => $request->slug,
            'value' => $request->value,
            'metros2' => $request->metros2,

//Bugeada la peticion
//            'lat' => $request->location['lat'],
//            'lng' => $request->location['lng'],
//            'pub_lat' => $request->publicLocation['latitude'],
//            'pub_lng' => $request->publicLocation['longitude'],

            'public_gps' => $request->public_gps,

            'estado_id' => $municipio->estado_id,
            'municipio_id' => $municipio->id,
            'CP' => $request->CP,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'num_ext' => $request->num_ext,
            'num_int' => $request->num_int,
            'description' => $request->description,

            //visits default 0 database
        ]);

        $property->facilities()->sync($this->facilities($request->facilities));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Property $property
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property) {
        //
    }

    public function facilities($request_facilities) {
        //Evitar poner algun número en instalaciones no contables
        $facilities = [];
        foreach ($request_facilities as $facility) {
            $fac = Facility::find($facility['id']);
            if ($fac->countable == "1") {
                $facilities[$fac->id] = ['count' => $facility['count']];
            } else {
                $facilities[$fac->id] = ['count' => -1];
            }
        }
        return $facilities;
    }
}
