<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PropertyRequest extends FormRequest {
    //en property observer agrego el id del usuario

    //forma 2
    public function __construct(Request $r) {
        $r->merge([
            'slug' => Str::slug($r->name),
            'cleanDescription' => preg_replace('/&\/?[^;]+(;|$)/', '', strip_tags($r->description))
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'status' => 'required',

//            Bugeado al editar, se manda pero no lo reconoce
//            'uso' => 'required',
            'category' => 'required',
            'name' => 'required',
            //revisar alpha dash para slug
            //agregado id para no crear conflicto en update
            //revisa que en la tabla properties, en la columna slug, sea unico
            'slug' => ['required', 'unique:properties,slug,' . $this->id],
            'value' => ['required', 'numeric'],

//            Bugeado al editar, se manda pero no lo reconoce
//            'location' => 'required',
            'estado' => 'required',
            'municipio' => 'required',
            'CP' => 'required|digits:5',
            'colonia' => 'required',
            'calle' => 'required',
            'num_ext' => 'required',
            'public_gps' => 'required',

            'description' => 'required',
            'cleanDescription' => ['required', 'min:30'],

            'facilities' => 'required',

            'images' => ['required', 'min:3']
        ];

        //Cambiar reglas dependiendo el metodo
//        if ($this->getMethod() == 'POST') {
//            $rules += ['password' => 'required|min:6'];
//        }

//        return [];
        return $rules;
    }

    public function attributes() {
        return [
        ];
    }

    public function messages() {
        return [
            'slug.unique' => 'slug.unique',
            'cleanDescription.min' => 'cD.min'

            //DEJADO DEL LADO DEL FRONT

//            'status.required' => 'Indica el contrato de la propiedad',
//            'uso.required' => 'Indica el uso de la propiedad',
//            'category.required' => 'Indica la categoría  de la propiedad',
//            'location.required' => 'Escoje la ubicación de la propiedad',
//            'name.required' => 'Dale un nombre a la propiedad',
//
//            'slug.unique' => 'Este nombre ya se esta usando',
//
//            'value.required' => 'Indica el precio de la propiedad',
//            'value.numeric' => 'Solo números',
//            'location.required' => 'Escoje la ubicación de la propiedad',


        ];
    }
}
