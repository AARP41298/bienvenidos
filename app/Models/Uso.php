<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uso extends Model {
    use HasFactory;

    //1 a *
    public function categories() {
        return $this->hasMany(Category::class);
    }

    //para las URL amigables
    public function getRouteKeyName() {
        return 'slug';
    }
}
