<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Localidad extends Model {
    use HasFactory;

    protected $table = 'localidades';

    //1 a *
    public function properties() {
        return $this->hasMany(Property::class);
    }

    //1 a * inversa
    public function municipio() {
        return $this->belongsTo(Municipio::class);
    }

    //para las URL amigables
    public function getRouteKeyName() {
        return 'slug';
    }
}
