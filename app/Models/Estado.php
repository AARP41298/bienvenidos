<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model {
    use HasFactory;

    //1 a *
    public function municipios() {
        return $this->hasMany(Municipio::class);
    }

    public function properties() {
        return $this->hasMany(Property::class);
    }

    //para las URL amigables
    public function getRouteKeyName() {
        return 'slug';
    }
}
