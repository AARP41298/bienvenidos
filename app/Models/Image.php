<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model {
    use HasFactory;

    protected $fillable = [
        'property_id',
        'url',
        'order'
    ];

    //evitar observer
    protected static function boot() {
        parent::boot();
        static::deleting(function ($image) {
            Storage::disk('public')->delete($image->url);
        });
    }

    //1 a * inversa
    public function property() {
        return $this->belongsTo(Property::class);
    }
}
