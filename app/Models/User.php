<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

use Spatie\Permission\Traits\HasRoles;
use LaravelAndVueJS\Traits\LaravelPermissionToVueJS;

class User extends Authenticatable {
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    //pemission
    use HasRoles;

    //permission to vue
    use LaravelPermissionToVueJS;

    //Para no usar un observer dedicado
    protected static function boot() {
        parent::boot(); // TODO: Change the autogenerated stub
        static::deleting(function ($user) {
            /*
            //TEST 1
            //no encadena property observer
            $user->properties()->delete();

            //TEST 2
            //funciona - revoltoso
            $properties = $user->properties;
            foreach ($properties as $property) {
                $images = $property->images;
                foreach ($images as $image) {
                    Storage::disk('public')->delete($image->url);
                    $image->delete();
                }
                $property->delete();
            }

            //TEST 3
            //no probado
            $user->properties->each->images->each->delete();
            */
            //TEST 4
            //like charm
            $user->properties->each->delete();
        });
        static::created(function ($user) {
            Contact::create([
                'user_id' => $user->id
            ]);
            $user->syncRoles(['Editor']);
        });
    }

    //1 a *
    public function properties() {
        return $this->hasMany(Property::class);
    }

    //1 a 1
    public function contact() {
        return $this->hasOne(Contact::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
}
