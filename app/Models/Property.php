<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Property extends Model {
    use HasFactory;

    //asignamient masivo
    protected $fillable = ['visits',
        'status_id',
        'category_id',
        'name',
        'slug',
        'value',
        'metros2',
        'lat',
        'lng',
        'pub_lat',
        'pub_lng',
        'public_gps',
        'estado_id',
        'municipio_id',
        'CP',
        'colonia',
        'calle',
        'num_ext',
        'num_int',
        'description'];

    //1 a *
    public function images() {
        return $this->hasMany(Image::class)->orderBy('order');
//        return $this->hasMany(Image::class);
    }

    //si funciona
    public function cover() {
        //Property::find(1)->cover()->geT()->toArray()[0]['url']
        //Property::find(1)->cover()->geT()->first()->url
        //return $this->hasMany(Image::class)->where('order', '=', 0)->geT()->first()->url;
        //return $this->hasOne(Image::class)->where('order', '=', 0);
        return $this->hasOne(Image::class)->orderBy('order');
    }

    //probando
    public function distance($lat, $lng) {
        return $lat;
    }

    //* a *
    public function facilities() {
        return $this->belongsToMany(Facility::class)->withPivot('count');
    }

    public function reports() {
        return $this->belongsToMany(Report::class)->withPivot('description');
    }

    //1 a * inversa
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function estado() {
        return $this->belongsTo(Estado::class);
    }

    public function municipio() {
        return $this->belongsTo(Municipio::class);
    }

    public function localidad() {
        return $this->belongsTo(Localidad::class);
    }

    public function category() {
        return $this->belongsTo(Category::class)->with('uso');
    }

    public function status() {
        return $this->belongsTo(Status::class);
    }

    //para las URL amigables
    public function getRouteKeyName() {
        return 'slug';
    }
}
