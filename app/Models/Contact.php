<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
    use HasFactory;

    //asignamiento masivo
    protected $fillable = [
        'user_id',
        'twitter',
        'facebook',
        'mail',
        'phone',
        'whatsapp',
        'web'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
