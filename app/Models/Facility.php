<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model {
    use HasFactory;

    //* a *
    public function properties() {
        //return $this->belongsToMany(Property::class,'facility_property','facility_id','property_id');
        return $this->belongsToMany(Property::class)->withPivot('count');
    }
}
