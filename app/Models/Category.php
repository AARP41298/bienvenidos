<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    use HasFactory;

    //1 a *
    public function properties() {
        return $this->hasMany(Property::class);
    }

    //1 a * inversa
    public function uso() {
        return $this->belongsTo(Uso::class);
    }
}
