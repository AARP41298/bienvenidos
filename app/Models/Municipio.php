<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model {
    use HasFactory;

    //1 a *
    public function localidades() {
        return $this->hasMany(Localidad::class);
    }

    public function properties() {
        return $this->hasMany(Property::class);
    }

    //1 a * inversa
    public function estado() {
        return $this->belongsTo(Estado::class);
    }

    //para las URL amigables
    public function getRouteKeyName() {
        return 'slug';
    }
}
