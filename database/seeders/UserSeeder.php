<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create([
            'name' => 'Aarón Rodríguez',
            'email' => 'aaron@hotmail.com',
            'password' => bcrypt('Pmz4Yvf2xrMzA2x'),
            //asignarme como admin
        ])->syncRoles(['Admin', 'Editor']);
        User::factory(9)->create();
    }
}
