<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $admin = Role::create(['name' => 'Admin']);
        $editor = Role::create(['name' => 'Editor']);
//        $baneado = Role::create(['name' => 'Baneado']);

        //WEB.PHP
        //vistas
        Permission::create(['name' => 'property.create', 'description' => 'Crear Propiedad'])->syncRoles([$editor]);
        Permission::create(['name' => 'property.edit', 'description' => 'Editar Propiedad'])->syncRoles([$editor, $admin]);
        //acciones
        Permission::create(['name' => 'property.store', 'description' => ''])->syncRoles([$editor]);//Redirecciona
        Permission::create(['name' => 'property.update', 'description' => ''])->syncRoles([$editor, $admin]);
        //vistas
        Permission::create(['name' => 'controlpanel.index', 'description' => 'Ver lista de propiedades'])->syncRoles([$editor]);
        Permission::create(['name' => 'controlpanel.contact', 'description' => 'Ver y editar información de contacto'])->syncRoles([$editor]);

        //API.PHP
        //acciones
        Permission::create(['name' => 'api.property.delete', 'description' => 'Eliminar propiedad API'])->syncRoles([$editor, $admin]);
        Permission::create(['name' => 'api.contact.update', 'description' => 'Actualizar Contacto API'])->syncRoles([$editor]);
        Permission::create(['name' => 'api.images.store', 'description' => 'Almacenar Imagen API'])->syncRoles([$editor]);
        Permission::create(['name' => 'api.images.destroy', 'description' => 'Eliminar Imagen API'])->syncRoles([$editor, $admin]);
        Permission::create(['name' => 'api.images.update', 'description' => 'Actualizar Orden de las Imagenes API'])->syncRoles([$editor]);
    }
}
