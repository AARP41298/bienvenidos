<?php

namespace Database\Seeders;

use App\Models\Image;
use App\Models\Property;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ImageSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker) {
        $imgPerProp = 1;
        for ($i = 1; $i <= Property::count(); $i++) {
            for ($j = 0; $j <= $imgPerProp; $j++) {
                $img = Image::create([
                    'url' => 'properties/' . $faker->image('public/storage/properties', rand(300, 1000), rand(300, 1000), null, false),
                    'property_id' => $i,
                    'order' => $j
                ]);

            }
        }
    }
}
