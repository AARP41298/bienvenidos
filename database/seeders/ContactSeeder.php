<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker) {
        $users = User::all();
        foreach ($users as $user) {
            //Ya creados vacios en el observer de usuario
            /*
            Contact::create([]);*/

            $contact = Contact::where('user_id', $user->id)->first();
            $contact->facebook = $this->fake($faker);
            $contact->twitter = $this->fake($faker);
            $contact->mail = $this->fake_mail($faker);
            $contact->phone = $this->fake_phone();
            $contact->whatsapp = $this->fake_phone();
            $contact->web = $this->fake_phone();
            $contact->save();

        }
    }

    public function fake($f) {
        if (rand(0, 1) == 0) {
            return $f->sentence();
        } else return null;
    }

    public function fake_phone() {
        if (rand(0, 1) == 0) {
            return rand(1111111111, 9999999999);
        } else return null;
    }

    public function fake_mail($f) {
        if (rand(0, 1) == 0) {
            return $f->safeEmail;
        } else return null;
    }
}

