<?php

namespace Database\Seeders;

use App\Models\Property;
use App\Models\Report;
use Illuminate\Database\Seeder;
use Faker\Generator;

class ReportSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker) {
        $razones = [
            'Otro',
            'Las fotos no corresponen con las instalaciones',
            'La información de contacto es incorrecta',
            'La dirección no corresponde con la ubicación del mapa',
        ];

        foreach ($razones as $razon) {
            Report::create([
                'reason' => $razon
            ]);
        }


        $properties = Property::all();
        foreach ($properties as $property) {
            for ($i = 0; $i < 2; $i++) {
                $r = rand(1, 4);
                $property->reports()->attach([
                    $r => ['description' => $faker->sentence()]
                ]);
            }
        }
    }
}
