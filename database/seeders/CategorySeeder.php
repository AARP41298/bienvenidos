<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        /*
                1 Domestico
                 2 Comercial
                3 Mixto
                4 Terreno
          */
        $categories = [
            (object)['name' => 'Casa',
                'uso_id' => 1],
            (object)['name' => 'Departamento',
                'uso_id' => 1],
            (object)['name' => 'Local',
                'uso_id' => 2],
            (object)['name' => 'Consultorio',
                'uso_id' => 2],
            (object)['name' => 'Taller',
                'uso_id' => 2],
            (object)['name' => 'Oficina',
                'uso_id' => 2],
            (object)['name' => 'Terreno',
                'uso_id' => 4],
        ];

        foreach ($categories as $category) {
            Category::create([
                'name' => $category->name,
                'slug' => Str::slug($category->name),
                'uso_id' => $category->uso_id
            ]);
        }
    }
}
