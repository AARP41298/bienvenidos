<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StatusSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $statuses = [
            'En Venta',
            'En Renta',
            'Vendido',
            'Rentando',
            'Oculto',
            'Baneado'
        ];
        foreach ($statuses as $status) {
            Status::create([
                'name' => $status,
                'slug' => Str::slug($status)
            ]);
        }
    }
}
