<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MunicipioSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::unprepared(file_get_contents('database/mexico_municipios.sql'));

        //valores duplicados en la DB del INEGI

        //san juan mixtepec
        //la localidad del 1218 hay que pasarla al 1217
        DB::table('municipios')->where('id','=',1218)->delete();

        //san pedro mixtepec
        //las localidades del 1328 al 1327
        DB::table('municipios')->where('id','=',1328)->delete();

        DB::unprepared(file_get_contents('database/municipios_slug.sql'));
    }
}
