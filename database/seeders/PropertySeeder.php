<?php

namespace Database\Seeders;

use App\Models\Facility;
use App\Models\Image;
use App\Models\Property;
use Illuminate\Database\Seeder;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $properties = Property::factory(800)->create();
        foreach ($properties as $property) {
            $a = 1;
            $b = 4;
            for ($i = 0; $i < 3; $i++) {
                $r = rand($a, $b);
                if (Facility::find($r)->countable == 1) {
                    $property->facilities()->attach([
                        $r => ['count' => rand(1, 8)]
                    ]);
                } else {
                    $property->facilities()->attach([
                        $r => ['count' => -1]
                    ]);
                }
                $a += 4;
                $b += 4;
            }

            /*
            $property->facilities()->attach([
                rand(1, 4) => ['count' => 10],
                rand(5, 8) => ['count' => 20],
                rand(9, 12) => ['count' => 30],
                rand(13, 16) => ['count' => 40],
            ]);*/
        }
    }
}
