<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */

    //extraido de https://developarts.com/db-estados-municipios-localidades-mexico
    public function run() {
        DB::unprepared(file_get_contents('database/mexico_estados.sql'));
        DB::unprepared(file_get_contents('database/estados_slug.sql'));

    }
}
