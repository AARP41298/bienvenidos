<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        //Empiezan los seeders

        // \App\Models\User::factory(10)->create();

        $this->call(EstadoSeeder::class);
        $this->call(MunicipioSeeder::class);
        //C:\ProgramData\MySQL\MySQL Server 8.0\my.ini
        //max_allowed_packet=18M
        //considero descartar el uso de localidades
        //$this->call(LocalidadSeeder::class);

        $this->call(FacilitySeeder::class);
        $this->call(UsoSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(StatusSeeder::class);

        $this->call(RoleSeeder::class);

        $this->call(UserSeeder::class);
        $this->call(ContactSeeder::class);

        //Debe haber una carpeta creada para guardar las imagenes
        Storage::deleteDirectory('public/properties');
        Storage::makeDirectory('public/properties');
        //php.ini memory_limit = 256M //maximo usa 50Mb en RAM
        $this->call(PropertySeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(ReportSeeder::class);
    }
}
