<?php

namespace Database\Seeders;

use App\Models\Facility;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FacilitySeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $instalaciones =
            [(object)['name' => 'Agua Potable',
                'icon' => 'fas fa-faucet',
                'countable' => 0],

                (object)['name' => 'Drenaje',
                    'icon' => 'fas fa-poo',
                    'countable' => 0],

                (object)['name' => 'Medios Baños',
                    'icon' => 'fas fa-toilet',
                    'countable' => 1],

                (object)['name' => 'Baños',
                    'icon' => 'fas fa-shower',
                    'countable' => 1],

                (object)['name' => 'Bañera',
                    'icon' => 'fas fa-bath',
                    'countable' => 0],


                (object)['name' => 'Alberca',
                    'icon' => 'fas fa-swimmer',
                    'countable' => 0],

                (object)['name' => 'Cocina',
                    'icon' => 'fas fa-utensils',
                    'countable' => 0],

                (object)['name' => 'Cochera',
                    'icon' => 'fas fa-warehouse',
                    'countable' => 0],
                (object)['name' => 'Espacios para auto',
                    'icon' => 'fas fa-car',
                    'countable' => 1],

                (object)['name' => 'Habitaciones',
                    'icon' => 'fas fa-bed',
                    'countable' => 1],

                (object)['name' => 'Gimnacio',
                    'icon' => 'fas fa-dumbbell',
                    'countable' => 0],

                (object)['name' => 'Consultorio',
                    'icon' => 'fas fa-user-md',
                    'countable' => 0],
                (object)['name' => 'Consultorios',
                    'icon' => 'fas fa-user-md',
                    'countable' => 1],

                (object)['name' => 'Consultorio Dental',
                    'icon' => 'fas fa-tooth',
                    'countable' => 0],

                (object)['name' => 'Taller',
                    'icon' => 'fas fa-hammer',
                    'countable' => 0],

                (object)['name' => 'Se permiten mascotas',
                    'icon' => 'fas fa-paw',
                    'countable' => 0],

                (object)['name' => 'Sala',
                    'icon' => 'fas fa-couch',
                    'countable' => 0],

                (object)['name' => 'Electricidad',
                    'icon' => 'far fa-lightbulb',
                    'countable' => 0],

                (object)['name' => 'Jardin',
                    'icon' => 'fas fa-leaf',
                    'countable' => 0],
            ];


        foreach ($instalaciones as $instalacion) {
            Facility::create([
                'name' => $instalacion->name,
                'slug' => Str::slug($instalacion->name),
                'icon' => $instalacion->icon,
                'countable' => $instalacion->countable,
            ]);
        }

    }
}
