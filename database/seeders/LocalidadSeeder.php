<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocalidadSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //archivo sql muy grande
        //C:\ProgramData\MySQL\MySQL Server 8.0\my.ini
        //max_allowed_packet=18M
        //DB::unprepared(file_get_contents('database/mexico_localidades_sin_duplicados.sql'));
        //Me encuentro que hay MUCHAS localidades repetidas en las localidades
        DB::unprepared(file_get_contents('database/mexico_localidades_municipios_duplicados_redirigidos_IGNORE.sql'));
        DB::unprepared(file_get_contents('database/localidades_slug.sql'));
    }
}
