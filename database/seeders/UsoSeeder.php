<?php

namespace Database\Seeders;

use App\Models\Uso;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsoSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $usos = [
            'Domestico',
            'Comercial',
            'Mixto',
            'Terreno'
        ];
        foreach ($usos as $uso) {
            Uso::create([
                'name' => $uso,
                'slug' => Str::slug($uso)
            ]);
        }
    }
}
