<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyReportTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('property_report', function (Blueprint $table) {
            $table->foreignId('property_id')
                ->references('id')->on('properties')->cascadeOnDelete();
            $table->foreignId('report_id')
                ->references('id')->on('reports');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('property_report');
    }
}
