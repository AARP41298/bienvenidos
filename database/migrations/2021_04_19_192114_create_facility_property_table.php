<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilityPropertyTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('facility_property', function (Blueprint $table) {

            $table->foreignId('facility_id')
                ->references('id')->on('facilities');
            $table->foreignId('property_id')
                ->references('id')->on('properties')->cascadeOnDelete();
            $table->string('count');

            //evitar repetir instalaciones en propiedaes
            $table->unique(['facility_id', 'property_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('facility_property');
    }
}
