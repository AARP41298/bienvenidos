<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->references('id')->on('users');
            $table->foreignId('status_id')
                ->references('id')->on('statuses');
            $table->foreignId('category_id')
                ->references('id')->on('categories');
            $table->string('name');
            $table->string('slug')->unique();
            $table->unsignedDecimal('value', 15, 2);
            $table->unsignedDecimal('metros2', 11, 1);


            $table->decimal('lat', 8, 6);
            $table->decimal('lng', 9, 6);
            $table->decimal('pub_lat', 8, 6);
            $table->decimal('pub_lng', 9, 6);


//            $table->foreignId('localidad_id')
//                ->references('id')->on('localidades');
            $table->foreignId('estado_id')
                ->references('id')->on('estados');
            $table->foreignId('municipio_id')
                ->references('id')->on('municipios');
            $table->string('calle');
            $table->string('num_ext');
            $table->string('num_int')->nullable();
            $table->string('colonia');
            $table->string('CP');

            $table->enum('public_gps', [0, 1]);

            $table->text('description');

            $table->unsignedInteger('visits')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('properties');
    }
}
