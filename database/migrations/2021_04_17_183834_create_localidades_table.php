<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalidadesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('localidades', function (Blueprint $table) {
            $table->id();
            $table->foreignId('municipio_id')
                ->references('id')->on('municipios');
            $table->string('name');
            $table->decimal('lat', 8, 6);
            $table->decimal('lng', 9, 6);
            $table->string('altitud', 6);

            //de la mano con insert ignore para evitar duplicados
            $table->unique(['municipio_id', 'name']);
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('localidades');
    }
}
