<?php

namespace Database\Factories;

use App\Models\Estado;
use App\Models\Localidad;
use App\Models\Municipio;
use App\Models\Status;
use App\Models\User;
use App\Models\Category;
use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PropertyFactory extends Factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Property::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $name = $this->faker->unique()->sentence();

        $lat = rand(18908450, 21425373) / 1000000;
        $lng = rand(-104051700, -97552673) / 1000000;

        $estado_id = Estado::inRandomOrder()->first()->id;
        $municipio_id = Municipio::where('estado_id', $estado_id)->inRandomOrder()->first()->id;

        return [
            'user_id' => User::all()->random()->id,
            'value' => rand(100000, 10000000),
            'description' => $this->faker->text(4000),

            //'category_id' => Category::all()->random()->id,
            'category_id' => Category::inRandomOrder()->limit(1)->get()->toArray()['0']['id'],

            'name' => $name,
            'slug' => Str::slug($name),
            'calle' => $this->faker->streetName(),
            'num_ext' => rand(1, 1000),
            'num_int' => rand(1, 5),
            'colonia' => $this->faker->city(),
            'CP' => $this->faker->postcode(),

            //'localidad_id' => Localidad::all()->random()->id,
            //'localidad_id' => Localidad::inRandomOrder()->limit(1)->get()->toArray()['0']['id'],

            //rand es MUCHO MÁS RAPIDO para el factory, escoge un ID que sabemos que hay
            //'localidad_id' => rand(1, 300690),
            //PERO como se eliminaron todos los duplicados gracias a insert ignore, no queda de otra más que consultar
            //'localidad_id' => Localidad::inRandomOrder()->limit(1)->get()->toArray()['0']['id'],
            /*
            'municipio_id' => rand(1, 2469),*/
            /*
            'municipio_id' => function () {
                do {
                    $n = rand(1, 2469);
                    //excluye los municipios que se eliminaro por estar duplicados
                } while (in_array($n, array(1218, 1328)));
                return $n;
            },*/
            'municipio_id' => $municipio_id,

//            'estado_id' => Estado::inRandomOrder()->limit(1)->get()->toArray()['0']['id'],
            'estado_id' => $estado_id,
            //faker propio, en algun lugar del centro de mexico
            'lat' => $lat,
            'lng' => $lng,
            'pub_lat' => $lat + 0.0004,
            'pub_lng' => $lng + 0.0004,
            'public_gps' => $this->faker->randomElement(['0', '1']),
            'visits' => rand(10, 4000),
            'metros2' => rand(5, 150),
            'status_id' => Status::inRandomOrder()->limit(1)->get()->toArray()['0']['id'],
        ];
    }
}
