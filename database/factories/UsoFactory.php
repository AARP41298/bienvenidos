<?php

namespace Database\Factories;

use App\Models\Uso;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Uso::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
