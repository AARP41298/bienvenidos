<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    Comentado para usar inertia head y mostrar el titulo de la pestaña--}}
{{--    <title>{{ config('app.name', 'Laravel') }}</title>--}}

<!-- Fonts -->
{{--    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">--}}

<!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- favicon -->
    <link rel="icon" type="image/png" href="{{ Storage::url('img/favicon.png') }}">

    <!-- Scripts -->
    @routes
    <script src="{{ mix('js/app.js') }}" defer></script>

    {{--    laravel permission to vue--}}
    <script type="text/javascript">
        window.Laravel = {
            csrfToken: "{{ csrf_token() }}",
            jsPermissions: {!! auth()->check()?auth()->user()->jsPermissions():0 !!}
        }
    </script>
</head>
<body class="font-sans antialiased">
@inertia
</body>
</html>
