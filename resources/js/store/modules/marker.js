export default {
    namespaced: true,
    state: {
        markers: []
    },
    mutations: {
        addMarkerX(state, m) {
            state.markers.push(m)
        },
        setMarkersX(state, markers) {
            state.markers = markers;
        },
        clearMarkersX(state) {
            state.markers = [];
        }
    },
    actions: {
        addMarkerX({commit}, m) {
            commit('addMarkerX', m)
        },
        setMarkersX({commit}, markers) {
            commit('setMarkersX', markers)
        },
        clearMarkersX({commit}) {
            commit('clearMarkersX')
        }

    }
}
