import {createStore} from 'vuex';
import marker from "./modules/marker";

const store = createStore({
    modules: {
        marker
    }
})

export default store;
