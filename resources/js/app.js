require('./bootstrap');


// Import modules...
import {createApp, h} from 'vue';
import {App as InertiaApp, plugin as InertiaPlugin} from '@inertiajs/inertia-vue3';
import {InertiaProgress} from '@inertiajs/progress';
//vuex
import store from "./store/store";
//permission
import LaravelPermissionToVueJS from 'laravel-permission-to-vuejs';


const el = document.getElementById('app');

createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({methods: {route}})
    .use(InertiaPlugin)
    .use(store)
    .use(LaravelPermissionToVueJS)
    .mount(el);

InertiaProgress.init({color: '#4B5563'});
