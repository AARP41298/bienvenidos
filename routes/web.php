<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\Admin\ControlPanelController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\Admin\RoleController;


use App\Http\Controllers\API\ApiPropertyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});
*/
/*
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');*/

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('paneldecontrol/propiedad/crear', [PropertyController::class, 'create'])->name('property.create');
    Route::get('paneldecontrol/propiedad/editar/{property}', [PropertyController::class, 'edit'])->name('property.edit');
    Route::post('propiedad', [PropertyController::class, 'store'])->name('property.store');//Redirecciona
    Route::put('propiedad/{property}', [PropertyController::class, 'update'])->name('property.update');

    //Route::get('usuario', [ControlPanelController::class, 'user'])->name('controlpanel.user');
    //Route::get('paneldecontrol/usuario', [ControlPanelController::class, 'user'])->name('controlpanel.user');
    Route::get('paneldecontrol', [ControlPanelController::class, 'home'])->name('controlpanel.home');
    Route::get('paneldecontrol/lista', [ControlPanelController::class, 'index'])->name('controlpanel.index');
    Route::get('paneldecontrol/admin/lista', [ControlPanelController::class, 'adminIndex'])->name('controlpanel.admin.index');
    Route::get('paneldecontrol/contacto', [ControlPanelController::class, 'contact'])->name('controlpanel.contact');
    Route::get('paneldecontrol/usuarios', [UserController::class, 'index'])->name('controlpanel.user.index');
    Route::get('paneldecontrol/editar/{user}', [UserController::class, 'edit'])->name('controlpanel.user.edit');
    Route::get('paneldecontrol/roles', [RoleController::class, 'index'])->name('controlpanel.role.index');


    Route::get('dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');


});
//Route::get('/get-permissions', function () {
//    $permissions = auth()->check() ? auth()->user()->jsPermissions() : null;
//    if ($permissions) {
//        $permissions = json_decode($permissions);
//    }
//    return response()->json(['permissions' => $permissions], 200);
//})->name('permissions');

Route::get('/', [PropertyController::class, 'index'])->name('property.index');
Route::get('/paginate/{size}', [PropertyController::class, 'paginate'])->name('property.paginate');


Route::get('propiedad/{property}', [PropertyController::class, 'show'])->name('property.show');
//Route::post('propiedad/{property}', [PropertyController::class, 'store'])->name('property.store');
Route::put('propiedad/visitas/{property}', [PropertyController::class, 'visits'])->name('property.visits');
Route::get('/buscar', [PropertyController::class, 'search'])->name('property.search');


Route::get('mexico', [EstadoController::class, 'index'])->name('estado.index');
Route::get('mexico/{estado}', [EstadoController::class, 'show'])->name('estado.show');
Route::get('mexico/{estado}/{municipio}', [EstadoController::class, 'municipio'])->name('estado.municipio');

Route::get('storage/', null)->name('storage');


Route::apiResource('instalaciones', FacilityController::class);
//Route::apiResource('propiedades', FacilityController::class);
