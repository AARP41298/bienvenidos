<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\API\ApiEstadoController;
use \App\Http\Controllers\API\ApiFacilityController;
use \App\Http\Controllers\API\ApiUsoController;
use \App\Http\Controllers\API\ApiPropertyController;
use \App\Http\Controllers\API\ApiContactController;
use \App\Http\Controllers\API\ApiImageController;
use \App\Http\Controllers\API\ApiUserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::group(['middleware' => ['auth:sanctum']], function () {
//    Route::delete('/propiedad/{property}', [ApiPropertyController::class, 'destroy'])->name('api.property.delete');
//});

Route::middleware('auth:sanctum')->group(function () {
    //agregado en http kernel ensure front para que funcione
    Route::delete('propiedad/{property}', [ApiPropertyController::class, 'destroy'])->name('api.property.delete');

    Route::put('contacto/{contact}', [ApiContactController::class, 'update'])->name('api.contact.update');
    Route::post('imagenes', [ApiImageController::class, 'store'])->name('api.images.store');
    Route::delete('imagenes/{image}', [ApiImageController::class, 'destroy'])->name('api.images.destroy');
    Route::put('imagenes/propiedad/{property}', [ApiImageController::class, 'update'])->name('api.images.update');
    Route::delete('usuario/{user}', [ApiUserController::class, 'destroy'])->name('api.user.delete');
    Route::put('usuario/{user}', [ApiUserController::class, 'update'])->name('api.user.update');

});


Route::get('estado', [ApiEstadoController::class, 'all'])->name('api.estado.all');
Route::get('estado/{estado}', [ApiEstadoController::class, 'municipios'])->name('api.estado.municipios');
Route::get('estado/{estado}/{municipio}', [ApiEstadoController::class, 'localidades']);

Route::get('instalacion', [ApiFacilityController::class, 'all'])->name('api.facility.all');
Route::get('uso', [ApiUsoController::class, 'all'])->name('api.uso.all');
Route::get('uso/{uso}', [ApiUsoController::class, 'categories'])->name('api.uso.categories');

Route::get('imagenes/{image}', [ApiImageController::class, 'show'])->name('api.images.show');
Route::get('imagenes/propiedad/{property}', [ApiImageController::class, 'property'])->name('api.images.property');
